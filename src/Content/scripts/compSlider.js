(($) => {
  const CompSlider = ($elem) => {
    const config = {
      selectors: {
        slider: '.comp-dollars-slider',
      },
      classes: {},
    };

    let $slider;

    const getElements = () => {
      $slider = $(config.selectors.slider);
    };
    const setUp = () => {
      $slider.each(function () {
        setTimeout(() => {
          $(this).slick({
            dots: true,
            arrows: true,
            infinite: true,
            useTransform: true,
            speed: 500,
            cssEase: 'ease',
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  dots: false,
                  arrows: false,
                },
              },
            ],
          });
        }, 20);
      });
    };

    const attachListeners = () => {};

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.comp-dollars-slider').each(() => {
      CompSlider($(this));
    });
  });
})(jQuery);
