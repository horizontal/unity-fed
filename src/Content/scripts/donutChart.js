(($) => {
  const DonutChart = ($elem) => {
    const config = {
      selectors: {
        chart: '.status-summary-chart',
        segment: '.donut-segment',
        currentPoints: '.current-points',
        goalPoints: '.status-summary-goals',
      },
      classes: {
        showModal: 'show-modal',
        hasModal: 'modal-showing',
      },
    };

    let $chart;
    let $segment;
    let $chartCurrent;
    let $chartGoal;
    let $goal;
    let $current;

    const getElements = () => {
      $chart = document.querySelector(config.selectors.chart);
      $segment = document.querySelector(config.selectors.segment);
      $chartCurrent = document.querySelector(config.selectors.currentPoints);
      $chartGoal = document.querySelector(config.selectors.goalPoints);
      $goal = $chart.dataset.goal;
      $current = $chart.dataset.current;
    };

    let i = 0;

    function format(num) {
      return ('' + num).replace(
        /(\d)(?=(?:\d{3})+(?:\.|$))|(\.\d\d?)\d*$/g,
        (m, s1, s2) => s2 || s1 + ',',
      );
    }

    function setGraph() {
      const graphComp = 2;
      const currentStatusStart = (i / $goal) * 100 - graphComp;
      const currentStatusFinish = 100 - (i / $goal) * 100 + graphComp;
      $segment.setAttribute(
        'stroke-dasharray',
        `${currentStatusStart} ${currentStatusFinish}`,
      );
      $chartCurrent.innerHTML = format(i);
      $chartGoal.innerHTML = format($goal);
    }

    function animate() {
      if (i < $current) {
        i += 10;
        setGraph();
      }
    }

    const setUp = () => {
      const setter = setInterval(animate, 10);
      if (i >= $current) {
        clearInterval(setter);
      }
    };

    const attachListeners = () => {};

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };

    return init();
  };

  $(function () {
    $('.status-summary-card').each(() => {
      DonutChart($(this));
    });
  });
})(jQuery);
