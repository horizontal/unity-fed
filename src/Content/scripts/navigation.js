(($) => {
  const Navigation = ($elem) => {
    const config = {
      selectors: {
        button: '.navbar-toggler',
      },
      classes: {
        navOpen: 'open',
      },
    };

    let $btn;
    let $header;
    let $navBar;
    let $navBarCollapse;
    let $window;

    const handleToggle = (e) => {
      e.preventDefault();
      $navBar.toggleClass(config.classes.navOpen);
      $header.toggleClass(config.classes.navOpen);
    };

    const handleResize = (e) => {
      if (
        $window.innerWidth > 769 &&
        $navBar.hasClass(config.classes.navOpen)
      ) {
        handleToggle(e);
        $navBarCollapse.removeClass('show');
        $btn.attr('aria-expanded', 'false');
      }
    };

    const getElements = () => {
      $header = $elem;
      $window = window;
      $navBar = $('.navbar', $header);
      $navBarCollapse = $('.navbar-collapse', $navBar);
      $btn = $(config.selectors.button, $header);
    };

    const setUp = () => {};

    const attachListeners = () => {
      $btn.on('click', handleToggle);
      $window.addEventListener('resize', handleResize);
    };

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.header').each(() => {
      Navigation($(this));
    });
  });
})(jQuery);
