(($) => {
  const BenefitsHero = ($elem) => {
    const config = {
      selectors: {
        hero: '.hero',
      },
      classes: {
        benefits: 'has-benefits',
      },
    };

    let $hero;

    const getElements = () => {
      $hero = document.querySelector(config.selectors.hero);
    };

    function handleBottomMargin() {
      if ($hero.nextElementSibling.classList.contains('benefits')) {
        $hero.classList.add(config.classes.benefits);
      }
    }

    const setUp = () => {
      handleBottomMargin();
    };

    const attachListeners = () => {};

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.hero').each(() => {
      BenefitsHero($(this));
    });
  });
})(jQuery);
