(($) => {
  const ActivationFormSubmit = ($elem) => {
    const config = {
      selectors: {
        form: '.create-password-form',
        formBtn: '.create-password-form .btn',
        password: 'password',
        confirmation: 'confirmation',
        validationItems: '.create-password-criteria-item',
        validated: '.create-password-criteria-item.checked',
        upCase: 'upperCase',
        lowCase: 'lowerCase',
        numCheck: 'numberCheck',
        specChar: 'specialCharacter',
        minChar: 'minCharacter',
        passwordToggle: '.js-password-toggle',
        validationError: '.validation-error-message',
        confirmationError: '.confirmation-error-message',
        requiredError: '.required-error-message',
      },
      classes: {
        error: 'error',
        checked: 'checked',
        errorMessage: 'error-message',
        errorInput: 'error-input',
        validMatch: 'valid-match',
      },
    };
    let form;
    let formBtn;
    let password;
    let confirmation;
    let validationItems;
    let upCase;
    let lowCase;
    let numCheck;
    let specChar;
    let minChar;
    let passwordToggle;
    let validationError;
    let confirmationError;
    let requiredError;

    const getElements = () => {
      form = document.querySelector(config.selectors.form);
      formBtn = document.querySelector(config.selectors.formBtn);
      password = document.getElementById(config.selectors.password);
      confirmation = document.getElementById(config.selectors.confirmation);
      validationItems = document.querySelectorAll(
        config.selectors.validationItems,
      );
      upCase = document.getElementById(config.selectors.upCase);
      lowCase = document.getElementById(config.selectors.lowCase);
      numCheck = document.getElementById(config.selectors.numCheck);
      specChar = document.getElementById(config.selectors.specChar);
      minChar = document.getElementById(config.selectors.minChar);
      passwordToggle = document.querySelectorAll(
        config.selectors.passwordToggle,
      );
      validationError = document.querySelector(
        config.selectors.validationError,
      );
      confirmationError = document.querySelector(
        config.selectors.confirmationError,
      );
      requiredError = document.querySelector(config.selectors.requiredError);
    };

    const uppercaseLetterCheck = (value) => {
      const re = /[A-Z]/g;
      if (!re.test(value.value.trim())) {
        upCase.classList.add(config.classes.error);
      } else {
        upCase.classList.add(config.classes.checked);
        upCase.classList.remove(config.classes.error);
      }
    };
    const lowercaseLetterCheck = (value) => {
      const re = /[a-z]/g;
      if (!re.test(value.value.trim())) {
        lowCase.classList.add(config.classes.error);
      } else {
        lowCase.classList.add(config.classes.checked);
        lowCase.classList.remove(config.classes.error);
      }
    };
    const numberCheck = (value) => {
      const re = /[0-9]/g;
      if (!re.test(value.value.trim())) {
        numCheck.classList.add(config.classes.error);
      } else {
        numCheck.classList.add(config.classes.checked);
        numCheck.classList.remove(config.classes.error);
      }
    };
    const specialCharacterCheck = (value) => {
      const re = /[.~`!#$%\@^&*+=\-\[\]\\';,{}|":<>\?]/g;
      if (!re.test(value.value.trim())) {
        specChar.classList.add(config.classes.error);
      } else {
        specChar.classList.add(config.classes.checked);
        specChar.classList.remove(config.classes.error);
      }
    };
    const minimumCharactersCheck = (value) => {
      if (value.value.trim().length < 8) {
        minChar.classList.add(config.classes.error);
      } else {
        minChar.classList.add(config.classes.checked);
        minChar.classList.remove(config.classes.error);
      }
    };
    const handlePasswordToggle = (e) => {
      const toggleInput = e.target.previousElementSibling;
      const type =
        toggleInput.getAttribute('type') === 'password' ? 'text' : 'password';
      if (toggleInput.value.trim() !== '') {
        toggleInput.setAttribute('type', type);
      }
      if (toggleInput.getAttribute('type') === 'text') {
        passwordToggle.innerHTML = 'hide';
      } else {
        passwordToggle.innerHTML = 'show';
      }
    };

    const handleValidateForm = (value) => {
      const validated = document.querySelectorAll(config.selectors.validated);
      if (value.value.trim() !== '') {
        uppercaseLetterCheck(value);
        lowercaseLetterCheck(value);
        numberCheck(value);
        specialCharacterCheck(value);
        minimumCharactersCheck(value);
      } else {
        validationItems.forEach((item) => {
          item.className = 'create-password-criteria-item';
        });
      }
      if (validated.length === validationItems.length) {
        password.classList.remove(config.classes.errorInput);
        validationError.classList.remove(config.classes.errorMessage);
        confirmation.classList.remove(config.classes.errorInput);
        confirmationError.classList.remove(config.classes.errorMessage);
      }
      if (password.value.trim() !== '') {
        requiredError.classList.remove(config.classes.errorMessage);
      }
    };

    const handlePasswordsMatch = (input1, input2) => {
      confirmation.classList.remove(config.classes.validMatch);
      if (input1.value !== input2.value) {
        confirmation.classList.add(config.classes.errorInput);
        confirmation.classList.remove(config.classes.validMatch);
        confirmationError.classList.add(config.classes.errorMessage);
      } else {
        confirmation.classList.remove(config.classes.errorInput);
        confirmationError.classList.remove(config.classes.errorMessage);
        confirmation.classList.add(config.classes.validMatch);
      }
    };

    const handleFormFail = () => {
      password.classList.add(config.classes.errorInput);

      confirmation.classList.remove(config.classes.validMatch);
      if (password.value.trim() === '') {
        requiredError.classList.add(config.classes.errorMessage);
      } else {
        validationError.classList.add(config.classes.errorMessage);
        confirmation.classList.add(config.classes.errorInput);
      }
    };

    let submitForm = false;

    const handleFormSubmit = () => {
      const validated = document.querySelectorAll(config.selectors.validated);
      if (
        validated.length === validationItems.length &&
        confirmation.classList.contains(config.classes.validMatch)
      ) {
        submitForm = true;
        console.log('form submitted');
      } else if (
        validated.length === validationItems.length &&
        !confirmation.classList.contains(config.classes.validMatch)
      ) {
        handlePasswordsMatch(password, confirmation);
        console.log('Password Mismatch');
      } else if (
        validated.length !== validationItems.length &&
        confirmation.classList.contains(config.classes.validMatch)
      ) {
        handleFormFail();
      } else if (
        validated.length !== validationItems.length &&
        !confirmation.classList.contains(config.classes.validMatch)
      ) {
        handleFormFail();
        console.log('Password Criteria not met');
      }
    };

    const setUp = () => {};

    const attachListeners = () => {
      password.addEventListener('keyup', () => {
        handleValidateForm(password);
      });
      confirmation.addEventListener('keyup', () => {
        handlePasswordsMatch(password, confirmation);
      });

      passwordToggle.forEach((trigger) => {
        trigger.addEventListener('click', handlePasswordToggle);
      });

      formBtn.addEventListener('click', (e) => {
        e.preventDefault();
        handleFormSubmit();
        if (submitForm) {
          form.submit();
        }
      });
    };

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('form.create-password-form').each(() => {
      ActivationFormSubmit($(this));
    });
  });
})(jQuery);
