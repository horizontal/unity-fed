(($) => {
  const CardFlip = ($elem) => {
    const config = {
      selectors: {
        button: '.js-dashboard-card__toggle',
        suggestionButton: '.js-suggestion-toggle',
      },
      classes: {
        cardFlipInit: 'dashboard-card-flipped',
        cardFlipEvent: 'dashboard-card',
      },
    };

    let $btn;
    let $flipWrapper;
    let $flipCardHeight;
    let $compDollars;
    let $compDollarsBtn;
    let $suggestionButton;

    const getElements = () => {
      $flipWrapper = $elem;
      $btn = $(config.selectors.button, $flipWrapper);
      $suggestionButton = $(config.selectors.suggestionButton);
      $flipCardHeight = $('.js-set-height');
      $compDollars = document.querySelector('.comp-dollars-card');
      $compDollarsBtn = $('.js-set-initial');
    };

    function handleHeight() {
      if (window.innerWidth > 767) {
        const heights = [];
        $flipCardHeight.each(function () {
          heights.push(this.offsetHeight);
        });
        $flipCardHeight.each(() => {
          $flipCardHeight.height(Math.max(...heights) + 'px');
        });
      } else {
        const compHeights = [];
        const compSetters = $('.comp-dollars-card')
          .closest('.dashboard-card')
          .find($flipCardHeight);
        compSetters.each(function () {
          compHeights.push(this.offsetHeight);
        });
        compSetters.each(() => {
          compSetters.height(Math.max(...compHeights) + 'px');
        });
      }
    }

    function handleFlip(e) {
      e.preventDefault();
      const trigger = e.target;
      trigger
        .closest(`.${config.classes.cardFlipEvent}`)
        .classList.toggle(config.classes.cardFlipInit);
    }

    function handleVisit() {
      $compDollars.closest(
        `.${config.classes.cardFlipEvent}`,
      ).dataset.visit = false;
    }

    function handleSuggestion() {
      const compSetters = $('.comp-dollars-card')
        .closest('.dashboard-card')
        .find($flipCardHeight);
      compSetters.css('height', 'auto');
      if (window.innerWidth < 767) {
        $suggestionButton
          .next('.comp-dollars-earnings-content')
          .toggleClass('open-suggestions');
        $suggestionButton.toggleClass('open-suggestions');
      }
      if (
        !$suggestionButton
          .next('.comp-dollars-earnings-content')
          .hasClass('open-suggestions')
      ) {
        setTimeout(() => {
          handleHeight();
        }, 300);
      }
    }

    const setUp = () => {
      setTimeout(() => {
        handleHeight();
      }, 30);
    };

    const attachListeners = () => {
      $btn.on('click', handleFlip);
      $compDollarsBtn.on('click', handleVisit);
      $suggestionButton.on('click', handleSuggestion);
    };

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.dashboard-card-wrapper').each(() => {
      CardFlip($(this));
    });
  });
})(jQuery);
