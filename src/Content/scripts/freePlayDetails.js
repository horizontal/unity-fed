(($) => {
  const FreePlayDetails = ($elem) => {
    const config = {
      selectors: {
        detailsTrigger: '.free-play-details-view',
        detailsView: '.free-play-details',
      },
      classes: {
        detailsOpen: 'open',
      },
    };

    let $detailsBtn;
    let $details;

    const getElements = () => {
      $detailsBtn = document.querySelector(config.selectors.detailsTrigger);
      $details = document.querySelector(config.selectors.detailsView);
    };

    const handleDetails = (e) => {
      $details.classList.toggle(config.classes.detailsOpen);
    };

    const setUp = () => {};

    const attachListeners = () => {
      $detailsBtn.addEventListener('click', handleDetails);
    };

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.free-play-details').each(() => {
      FreePlayDetails($(this));
    });
  });
})(jQuery);
