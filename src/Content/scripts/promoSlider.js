(($) => {
  const PromoSlider = ($elem) => {
    const config = {
      selectors: {
        slider: '.promotions-slider',
      },
      classes: {},
    };

    let $slider;

    const getElements = () => {
      $slider = $(config.selectors.slider);
    };
    const setUp = () => {
      $slider.each(function () {
        setTimeout(() => {
          $(this).slick({
            dots: true,
            arrows: true,
            infinite: true,
            useTransform: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            speed: 500,
            cssEase: 'ease',
            responsive: [
              {
                breakpoint: 769,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: true,
                },
              },
            ],
          });
        }, 20);
      });
    };

    const attachListeners = () => {};

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.promotions-slider').each(() => {
      PromoSlider($(this));
    });
  });
})(jQuery);
