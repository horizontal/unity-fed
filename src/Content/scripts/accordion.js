(($) => {
  const AccordionOpen = ($elem) => {
    const config = {
      selectors: {
        accordionTrigger: '.accordion-question',
      },
      classes: {
        accOpen: 'open',
        accOpenBtnConditional: 'accordion-question',
      },
    };

    let $accBtn;

    const getElements = () => {
      $accBtn = document.querySelectorAll(config.selectors.accordionTrigger);
    };

    const handleAccordion = (e) => {
      const button = e.target;
      if (button.classList.contains(config.classes.accOpenBtnConditional)) {
        button.classList.toggle(config.classes.accOpen);
        button.nextElementSibling.classList.toggle(config.classes.accOpen);
      }
    };

    const setUp = () => {};

    const attachListeners = () => {
      $accBtn.forEach((accBar) => {
        accBar.addEventListener('click', handleAccordion);
      });
    };

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.accordion').each(() => {
      AccordionOpen($(this));
    });
  });
})(jQuery);
