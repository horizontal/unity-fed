(($) => {
  const AlertBannerMargin = ($elem) => {
    const config = {
      selectors: {
        banner: '.alert-banner',
      },
      classes: {},
    };

    let $alertBanner;

    const getElements = () => {
      $alertBanner = document.querySelectorAll(config.selectors.banner);
    };

    function handleBottomMargin() {
      if ($alertBanner.length > 0) {
        $alertBanner.forEach((alert) => {
          const nextEl = alert.nextElementSibling;
          const nextElPadding = window
            .getComputedStyle(nextEl)
            .getPropertyValue('padding-top');
          if (parseInt(nextElPadding, 10) < alert.offsetHeight) {
            nextEl.style.paddingTop = `${alert.offsetHeight}px`;
          }
        });
      }
    }

    const setUp = () => {
      handleBottomMargin();
    };

    const attachListeners = () => {};

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.alert-banner').each(() => {
      AlertBannerMargin($(this));
    });
  });
})(jQuery);
