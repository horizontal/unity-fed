(($) => {
  const Modal = ($elem) => {
    const config = {
      selectors: {
        openButton: '.js-modal-open',
        closeButton: '.js-modal-close',
      },
      classes: {
        showModal: 'show-modal',
        hasModal: 'modal-showing',
      },
    };

    let $openBtn;
    let $closeBtn;
    let $modal;
    let $hero;
    let $body;
    let $document;

    const getElements = () => {
      $hero = $elem;
      $document = $(document);
      $body = $('body');
      $modal = $('.dashboard-hero-modal-container', $hero);
      $openBtn = $(config.selectors.openButton, $hero);
      $closeBtn = $(config.selectors.closeButton, $modal);
    };

    function handleShow() {
      setTimeout(() => {
        $modal.addClass(config.classes.showModal);
        $body.addClass(config.classes.hasModal);
        $closeBtn.focus();
      }, 0);
    }
    function handleHide() {
      $modal.removeClass(config.classes.showModal);
      $body.removeClass(config.classes.hasModal);
    }
    function handleClickAway(e) {
      if ($modal.hasClass(config.classes.showModal)) {
        if (!$(e.target).closest('.dashboard-hero-modal').length) {
          handleHide(e);
        }
      }
    }

    const setUp = () => {};

    const attachListeners = () => {
      $openBtn.on('click', handleShow);
      $closeBtn.on('click', handleHide);
      $document.on('click', handleClickAway);
      $document.on('keydown', (e) => {
        if ($modal.hasClass(config.classes.showModal)) {
          if (e.which === 27) {
            handleHide(e);
          }
        }
      });
    };

    const init = () => {
      getElements();
      setUp();
      attachListeners();
    };
    return init();
  };

  $(function () {
    $('.dashboard-hero').each(() => {
      Modal($(this));
    });
  });
})(jQuery);
